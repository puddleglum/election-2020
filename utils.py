all_states = {
    'alabama',
    'alaska',
    'arizona',
    'arkansas',
    'california',
    'colorado',
    'connecticut',
    'delaware',
    'florida',
    'georgia',
    'hawaii',
    'idaho',
    'illinois',
    'indiana',
    'iowa',
    'kansas',
    'kentucky',
    'louisiana',
    'maine',
    'maryland',
    'massachusetts',
    'michigan',
    'minnesota',
    'mississippi',
    'missouri',
    'montana',
    'nebraska',
    'nevada',
    'new-hampshire',
    'new-jersey',
    'new-mexico',
    'new-york',
    'north-carolina',
    'north-dakota',
    'ohio',
    'oklahoma',
    'oregon',
    'pennsylvania',
    'rhode-island',
    'south-carolina',
    'south-dakota',
    'tennessee',
    'texas',
    'utah',
    'vermont',
    'virginia',
    'washington',
    'west-virginia',
    'wisconsin',
    'wyoming'
}

swing = {
    'arizona',
    # 'florida',
    'georgia', 'michigan', 'minnesota',
    # 'north-carolina',
    'pennsylvania', 'wisconsin',
}

great_plains = {
    'idaho', 'wyoming', 'south-dakota', 'north-dakota', 'montana',
    'utah', 'kansas', 'oklahoma', 'nebraska', 'iowa',
}

south = {
    'alabama', 'georgia', 'mississippi', 'louisiana', 'arizona',
    'arkansas', 'tennessee', 'south-carolina', 'florida', 'kentucky',
    'missouri', 'texas', 'west-virginia',
}

mid_atlantic = {
    'new-york', 'new-jersey', 'maryland', 'delaware', 'north-carolina',
    'virginia',
}

new_england = {
    'vermont', 'massachusetts', 'rhode island', 'connecticut',
    'new-hampshire', 'maine', 'rhode-island',
}

west_coast = {
    'california', 'oregon', 'washington', 'hawaii'
}

mountain = {
    'arizona', 'colorado', 'idaho', 'montana', 'nevada',
    'new-mexico', 'utah', 'wyoming', 'alaska'
}

great_lakes = {
    'illinois', 'indiana', 'michigan', 'minnesota', 'new-york',
    'ohio', 'pennsylvania', 'wisconsin',
}

multi_counties = {
    45071, 47119, 45075, 45079, 45083, 45085, 41003, 41005, 47157, 47175, 41033, 41051, 39009, 39035, 39043, 39047, 39049, 39061, 39077, 37035, 37037, 39093, 39095, 35001, 39099, 39101, 39103, 39107, 37067, 39119, 39123, 35031, 39133, 37085, 35041, 35043, 39139, 39141, 35049, 37097, 33001, 39145, 39151, 39153, 37105, 33009, 33011, 33013, 35061, 33015, 39155, 39157, 37125, 37147, 37161, 37181, 37183, 31051, 29007, 51550, 29029, 29047, 27003, 27007, 27019, 27033, 29095, 29099, 27053, 25005, 31153, 25009, 25011, 25013, 25015, 25017, 25021, 25023, 25025, 51650, 25027, 23011, 27123, 27131, 51710, 29183, 29189, 27145, 27163, 21019, 29225, 51800, 21097, 17005, 21111, 21113, 17031, 17037, 17043, 15003, 17053, 17089, 17093, 17097, 17111, 13021, 17119, 21215, 21229, 17143, 13059, 13063, 13067, 13073, 17175, 17179, 13089, 9001, 9003, 17197, 9005, 9007, 13103, 9009, 17201, 13113, 13117, 13121, 13135, 13151, 13185, 48021, 48027, 48029, 13215, 13217, 48039, 5033, 13227, 48055, 48085, 48091, 44007, 5101, 48113, 1009, 48121, 1019, 5131, 48141, 48143, 42003, 48153, 42011, 48157, 42019, 42021, 42027, 48177, 42041, 48187, 48201, 1101, 48209, 40017, 48215, 42079, 40037, 42089, 42091, 42097, 42101, 42129, 42133, 48283, 48287, 48289, 36005, 36007, 40109, 36027, 36029, 40131, 36043, 36047, 34003, 34005, 34007, 36055, 36057, 36059, 34013, 36061, 36063, 34015, 34017, 36069, 34021, 34023, 34025, 36075, 34029, 34031, 36081, 34035, 36083, 34037, 34039, 36091, 32003, 36103, 36107, 32019, 36119, 48409, 48429, 48439, 48453, 48459, 48493, 48497, 48499, 28023, 28049, 26005, 28089, 24003, 24005, 28105, 24013, 24021, 24025, 24027, 24031, 24033, 26081, 26099, 22005, 22007, 26105, 26117, 26125, 22033, 22047, 26145, 22051, 22057, 26157, 26161, 26163, 22071, 22089, 22095, 22097, 18009, 18011, 22109, 18025, 22121, 16001, 18067, 20117, 20121, 18085, 18091, 18097, 20145, 18109, 18143, 55017, 12011, 55027, 12023, 12031, 55053, 55057, 12057, 53017, 12059, 12065, 12069, 55079, 12071, 53033, 12073, 55081, 12083, 53045, 12086, 53053, 12095, 55103, 8001, 53057, 12099, 55105, 8005, 53061, 12103, 12105, 51019, 53067, 8013, 12109, 53071, 53073, 12115, 55127, 55133, 51041, 8035, 55139, 8037, 55141, 6001, 51059, 49011, 51061, 8059, 6013, 49023, 6019, 6021, 49035, 6029, 49039, 51087, 6033, 6037, 6039, 51095, 49049, 8093, 47005, 47011, 47013, 4007, 6057, 6059, 4013, 6061, 4015, 6065, 6067, 4019, 4021, 6071, 6073, 4025, 4027, 6075, 6077, 24510, 6081, 6085, 6087, 6095, 51153, 6097, 45013, 45015, 6107, 45019, 6111, 6113, 45029, 51177, 45035, 45041, 47089, 45045
}

winner_parties = {
    1920: 'republican',
    1924: 'republican',
    1928: 'republican',
    1932: 'democratic',
    1936: 'democratic',
    1940: 'democratic',
    1944: 'democratic',
    1948: 'democratic',
    1952: 'republican',
    1956: 'republican',
    1960: 'democratic',
    1964: 'democratic',
    1968: 'republican',
    1972: 'republican',
    1976: 'democratic',
    1980: 'republican',
    1984: 'republican',
    1988: 'republican',
    1992: 'democratic',
    1996: 'democratic',
    2000: 'republican',
    2004: 'republican',
    2008: 'democratic',
    2012: 'democratic',
    2016: 'republican',
}

winner_parties_1920_slug = {
    k: {'republican': 'gop', 'democratic': 'dem'}.get(v)
    for k, v in winner_parties.items()
}

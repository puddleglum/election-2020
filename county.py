import io
from pathlib import Path
from datetime import datetime
import json

import requests
import click
import dateutil.parser
import numpy as np
import pandas as pd
import toolz.curried as _
import larc
import larc.common as __

import utils

log = larc.logging.new_log(__name__)

COUNTY_POP_URL = (
    'https://www2.census.gov/programs-surveys/'
    'popest/datasets/2010-2019/counties/totals/co-est2019-alldata.csv'
)

COUNTY_POP_PATH = Path('county-populations.csv')

@_.memoize
def county_populations():
    if COUNTY_POP_PATH.exists():
        return pd.read_csv(COUNTY_POP_PATH)

    df = _.pipe(
        requests.get(COUNTY_POP_URL).text,
        io.StringIO,
        pd.read_csv,
    )

    df['fips'] = (
        df['STATE'].apply(str) + (
            df['COUNTY'].apply(str)
            .map(lambda s: s.zfill(3))
        )
    ).apply(int)

    df = df[df['COUNTY'] > 0]
    df.to_csv(str(COUNTY_POP_PATH))

    return df

def county_pop_lut(year=2019):
    df = county_populations()
    return df.groupby('fips')[f'POPESTIMATE{year}'].sum()
    
@_.memoize
def pres_data_historical():
    df = pd.read_csv(
        _.pipe(
            Path(
                'us-presidential-county-1960-2016',
                'data', 'processed',
                'full-us-presidential-counties-1960-2016.tsv',
            ),
            str,
        ),
        delimiter='\t',
    )
    df.columns = df.columns.str.replace('.', '_')

    return df

NYT_PATH = Path('USElection2020-NYT-Results', 'data')

def county_data_2020_path(max_dt):
    def path_dt(p):
        return datetime.strptime(p.name, '%Y-%m-%d %H-%M-%S')

    def max_dt_path_filter(p):
        return path_dt(p) < max_dt

    return _.pipe(
        NYT_PATH.glob('*'),
        _.filter(lambda p: __.maybe_dt(p.name)),
        _.filter(max_dt_path_filter),
        __.max(key=path_dt),
        _.do(print),
    )

@_.memoize
def raw_pres_data_2020(max_dt=None):
    max_dt = max_dt or datetime.now()
    max_dt = __.maybe_dt(max_dt)
    path = county_data_2020_path(max_dt) / 'presidential' / 'presidential.csv'
    log.info(f'Found path: {path}')

    return pd.read_csv(str(path))

@_.memoize
def pres_data_2020(max_dt=None):
    max_dt = max_dt or datetime.now()
    orig = raw_pres_data_2020(max_dt)

    # return orig

    results_columns = _.pipe(
        orig.columns,
        _.filter(lambda s: s.startswith('results_')),
        _.filter(lambda s: 'absentee' not in s),
        tuple,
    )
    
    # orig.rename(
    #     columns={'fips': 'county.fips', 'state': 'state.name',
    #              'name': 'county.name'},
    #     inplace=True,
    # )

    trump_v = orig.results_trumpd
    biden_v = orig.results_bidenj
    other_v = orig[
        set(results_columns) - {'results_bidenj', 'results_trumpd'}
    ].sum(axis=1)
    total_v = trump_v + biden_v + other_v

    trump_p = trump_v / total_v
    biden_p = biden_v / total_v
    other_p = other_v / total_v

    N = len(orig)

    def dup(s):
        return pd.concat([s] * 3).values

    data = {
        'county_fips': dup(orig.fips), 
        'state_name': dup(orig.state.str.lower().str.replace('-', ' ')),
        'county_name': dup(orig.name.str.lower()),
        'county_total_count': dup(orig.votes),
        'year': [2020] * (N * 3),
        'party': ['D'] * N + ['O'] * N + ['R'] * N,
        'vote_count': pd.concat([biden_v, other_v, trump_v]),
        'vote_percent': pd.concat([biden_p, other_p, trump_p]),
        'is_national_winner': [True] * N + [False] * N + [False] * N,
        'national_party_count': [biden_v.sum()] * (N * 3),
        'national_count': (
            [biden_v.sum() + trump_v.sum() + other_v.sum()] * (N * 3)
        ),
    }

    # print(data)

    df = pd.DataFrame(data)
    df['national_party_percent'] = (
        df['national_party_count'] / df['national_count']
    )

    return df

CAND_COLUMNS = [
    'key', 'first', 'last', 'display', 'state', 'state_code', 'party'
]

key_translator = {
    'christophes': 'christophec',
    'kean_t': 'keant',
    'lathamj': 'lathamr',
    'dirksen_londriganb': 'londriganb',
    'marxb': 'marxw',
    'paul_moranj': 'moranj',
    'oconnor_muerih': 'muerih',
    'ramirez_mukherjeev': 'mukherjeev',
    'stotts_pearsone': 'pearsone',
    'sindts': 'pendergast_sindts',
}

def get_candidate_data(house_json):
    for race in house_json['data']['races']:
        s, s_code = race['state_slug'][0], race['state_id'][0]
        for c in race['candidates']:
            key = c['candidate_key'][0].replace('-', '_')
            data = (
                key_translator.get(key, key),
                c['first_name'][0],
                c['last_name'][0],
                c['name_display'][0],
                s, s_code,
                c['party_id'][0]
            )
            yield data

@_.memoize
def candidates():
    path = NYT_PATH / 'latest' / 'house.json'
    log.info(f'Reading candidate data at {path}')

    data = get_candidate_data(json.loads(path.read_text()))

    df = pd.DataFrame(data, columns=CAND_COLUMNS)
    df['column'] = df.key.map(lambda k: f'results_{k}')

    return df

def resolve_candidates(house_df, candidates):
    rescols = results_columns(house_df)
    return candidates[candidates.column.isin(rescols)]

@_.memoize
def raw_house_data_2020(max_dt=None):
    max_dt = max_dt or datetime.now()
    max_dt = __.maybe_dt(max_dt)
    path = county_data_2020_path(max_dt) / 'house' / 'house.csv'
    log.info(f'Found path: {path}')

    return pd.read_csv(str(path))

def results_columns(df):
    return _.pipe(
        df.columns,
        _.filter(lambda s: s.startswith('results_')),
        _.filter(lambda s: 'absentee' not in s),
        list,
    )

@_.memoize
def house_data_2020(max_dt=None):
    max_dt = max_dt or datetime.now()
    orig = raw_house_data_2020(max_dt)

    cand = resolve_candidates(orig, candidates())
    orig = orig[orig.leader_party_id.isin({'republican', 'democrat'})]

    def rescols(row):
        return _.pipe(
            row.dropna().index,
            _.filter(lambda s: s.startswith('results_')),
            _.filter(lambda s: 'absentee' not in s),
            list,
        )

    def collapse_votes(filter_df):
        return (
            orig[(
                filter_df
                .column
                .drop_duplicates()
            )]
            .sum(axis=1)
        )

    gop_votes = collapse_votes(cand[cand.party == 'republican'])

    dem_votes = collapse_votes(cand[cand.party == 'democrat'])

    other_votes = collapse_votes(
        cand[~cand.party.isin({'republican', 'democrat'})]
    )

    total_votes = gop_votes + dem_votes + other_votes

    gop_p = gop_votes / total_votes
    dem_p = dem_votes / total_votes
    other_p = other_votes / total_votes
    
    data = {
        'county_fips': orig.fips,
        'fips': orig.fips,
        'id': orig.race_id_race,
        'state_name': orig.state_slug_race,
        'county_name': orig.name.str.lower(),
        'party': orig.leader_party_id,
        'gop_votes': gop_votes,
        'dem_votes': dem_votes,
        'other_votes': other_votes,
        'total_votes': total_votes,
        'gop_percent': gop_p,
        'dem_percent': dem_p,
        'other_percent': other_p,
    }

    # print(data)

    df = pd.DataFrame(data)
    # df['national.party.percent'] = (
    #     df['national_party_count'] / df['national_count']
    # )

    return df

def resolve_fips(df, df_2020):
    return (
        df[df['county_fips'].isin(df_2020['county_fips'])],
        df_2020[df_2020['county_fips'].isin(df['county_fips'])]
    )
    return (
        df[df['county_fips'].isin(df_2020['fips'])],
        df_2020[df_2020['fips'].isin(df['county_fips'])]
    )

def choices(df):
    choices = df[
        df
        .groupby(['year', 'county_fips'])['vote_count']
        .transform(np.max) == df['vote_count']
    ].drop_duplicates(subset=['county_fips', 'year'], keep='first')

    choices['elections'] = (
        choices
        .groupby('county_fips')
        .year
        .transform(len)
    )
    choices['first_election'] = (
        choices
        .groupby('county_fips')
        .year
        .transform(np.min)
    )

    return choices

def weights_series(weights):
    return _.pipe(
        weights,
        __.vcall(zip),
        __.vcall(lambda years, weights: pd.Series(weights, index=years)),
        # lambda weights: weights / weights.sum(),
    )

weights = _.pipe(
    [
        (1920, 2),
        (1924, 1),
        (1928, 1),
        (1932, 2),
        (1936, 1),
        (1940, 1),
        (1944, 1),
        (1948, 1),
        (1952, 2),
        (1956, 1),
        (1960, 2),
        (1964, 1),
        (1968, 2),
        (1972, 1),
        (1976, 2),
        (1980, 3),
        (1984, 1),
        (1988, 1),
        (1992, 3),
        (1996, 1),
        (2000, 2),
        (2004, 1),
        (2008, 2),
        (2012, 1),
        (2016, 2),
        (2020, 3),
    ],
    weights_series,
)

def times_correct_fips(choices, weights=None):
    bw = choices[choices['is_national_winner']].copy()
    bw['winner_norm'] = bw['is_national_winner'] / bw['elections']

    if weights is not None:
        bw['winner_norm'] = (
            bw['is_national_winner'] * bw['year'].map(weights)
        )
        bw['winner_norm'] /= bw['first_election'].map(
            lambda year: weights.loc[year:].sum()
        )

    return (
        bw
        .groupby('county_fips')
        ['winner_norm']
        .sum()
        .sort_values()
    )

def bw_check(state, top_n=5):
    df, df2020 = resolve_fips(pres_data_historical(), pres_data_2020())
    df = df[df['state_name'] == state]

    fips = times_correct_fips(choices(df))

    results = df2020[df2020.fips.isin(fips[-top_n:].index)]

    return len(
        results[results['results_trumpd'] > results['results_bidenj']]
    ) / top_n

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

def trend_line(ax, X, Y, color, n=10, lw=5):
    from scipy import interpolate

    # X, Y = X.values, Y.values

    nans = np.isnan(X) | np.isnan(Y)
    X, Y = X[~nans], Y[~nans]

    # print(zero_or_one.sum())
    # print(len(X))
    # print(len(X))
    
    # sort_indexes = np.argsort(X)
    Xs = np.array_split(X, n)
    Ys = np.array_split(Y, n)

    trend_x = [x.mean() for x in Xs]
    trend_y = [y.mean() for y in Ys]
            

    # data = np.array(zip(trend_x, trend_y))

    tck, u = interpolate.splprep([trend_x, trend_y], s=0)
    unew = np.arange(0, 1.01, 0.01)
    out = interpolate.splev(unew, tck)

    ax.plot(out[0], out[1], color=color, lw=lw)
    # ax.plot(out[0], out[1], color=lighten_color(color, 2.0), lw=1)
    ax.plot(out[0], out[1], color='black', lw=1)

    # for (x, y) in zip(Xs, Ys):
    #     z = np.polyfit(x, y, 1)
    #     p = np.poly1d(z)
    #     plt.plot(x, p(x), color=color)

def viz_counties(max_dt=None, states=None, which='both', trend=True,
                 ax=None, **plot_kw):
    max_dt = max_dt or datetime.now()

    house = house_data_2020()

    house = house[~(house.fips.isin(utils.multi_counties))]
    house = house[~(house.total_votes == 0)]
    house = house[~(house.gop_votes == 0)]
    house = house[~(house.dem_votes == 0)]

    raw_2020 = raw_pres_data_2020(max_dt)

    if states:
        raw_2020 = raw_2020[raw_2020.state.isin(states)]
        house = house[house.state_name.isin(states)]

    total_pres_votes = raw_2020.groupby('fips').votes.sum()

    trump_perc_map = (
        raw_2020.groupby('fips').results_trumpd.sum() / total_pres_votes
    )
    biden_perc_map = (
        raw_2020.groupby('fips').results_bidenj.sum() / total_pres_votes

    )

    trump_votes_map = (
        raw_2020.groupby('fips').results_trumpd.sum()
    )
    trump_votes = house.fips.map(
        raw_2020.groupby('fips').results_trumpd.sum()
    )

    biden_votes_map = (
        raw_2020.groupby('fips').results_bidenj.sum()
    )
    biden_votes = house.fips.map(
        raw_2020.groupby('fips').results_bidenj.sum()
    )
    

    # pops = house.fips.map(county_pop_lut())
    pops = house.total_votes
    min_pop = pops.min()
    # print(min_pop)
    pops = pops - min_pop
    max_pop = pops.max()
    pops = pops / max_pop
    # print(pops)
    sizes = (pops * 500) + 1
    # print(sizes)
    # alpha = (1 - (pops * 0.8)) + 0.2
    alpha = (1 - pops) * 0.7 + 0.3
    # print(alpha)

    data = {
        'trump': {
            'X': house.gop_votes,
            # 'X': house.gop_percent,
            'color': 'red',
            'line-color': '#ff8888',
        },
        'biden': {
            'X': house.dem_votes,
            # 'X': house.dem_percent,
            'color': 'blue',
            'line-color': '#8888ff',
        },
    }
    # data['trump']['Y'] = house.fips.map(trump_perc_map)# - data['trump']['X']
    # data['biden']['Y'] = house.fips.map(biden_perc_map)# - data['biden']['X']
    data['trump']['Y'] = house.fips.map(trump_votes_map)
    data['biden']['Y'] = house.fips.map(biden_votes_map)

    if which == 'both':
        candidates = ['trump', 'biden']
    else:
        candidates = [which]

    import matplotlib.pyplot as plt
    from matplotlib.colors import to_rgb
    import mplcursors
    
    fig, ax = plt.subplots()

    viz_data = []
    for c in candidates:
        X, Y = data[c]['X'].values, data[c]['Y'].values
        sort_indexes = np.argsort(X)

        X = X[sort_indexes]
        Y = Y[sort_indexes]

        # states = house.state_name.values[sort_indexes]
        # counties = house.county_name.values[sort_indexes]
        tvotes = trump_votes.map(str)

        bvotes = biden_votes.map(str)

        fstr = '{:02f}%'.format
        
        labels = (
            house.state_name + '\n' +
            house.county_name + ' (' + house.fips.map(str) + ')\n' +
            house.id + '\n' +
            'GOP votes: ' + house.gop_votes.map(int).map(str) +
            '(' + house.gop_percent.map(fstr) + ')\n' +
            'Dem votes: ' + house.dem_votes.map(int).map(str) +
            '(' + house.dem_percent.map(fstr) + ')\n' +
            'Trump votes: ' + tvotes +
            '(' + data['trump']['Y'].map(fstr) + ')\n' +
            'Biden votes: ' + bvotes +
            '(' + data['biden']['Y'].map(fstr) + ')'
        ).values[sort_indexes]

        sz = sizes.values[sort_indexes]
        alp = alpha.values[sort_indexes]

        # zero_or_one = (X < 0.01) | (X > 0.99)
        # X, Y = X[~zero_or_one], Y[~zero_or_one]
        # sz = sz[~zero_or_one]
        # alp = alp[~zero_or_one]

        viz_data.append(
            (X, Y, data[c]['color'], data[c]['line-color'], sz, alp, labels)
        )

    for X, Y, color, _c, sz, alp, labels in viz_data:
        r, g, b = to_rgb(color)
        # print(r, g, b)
        # print(alp)
        # print(sizes)
        points = ax.scatter(
            X, Y,
            c=[(r, g, b, a) for a in alp],
            s=sz,
            **plot_kw
        )

        cursor = mplcursors.cursor(points)
        cursor.connect(
            'add', lambda sel: sel.annotation.set_text(
                labels[sel.target.index]
            )
        )

    if trend:
        for X, Y, _c, color, sz, alp, labels in viz_data:
            trend_line(ax, X, Y, color)
            # z = np.polyfit(X, Y, 1)
            # p = np.poly1d(z)
            # plt.plot(X, p(X), color=color)

    ax.plot([0, 1], [0, 1], '--', color='black')
    return ax

def anomaly(df, state, county, degree=3):
    votes = (
        df
        .groupby(['state_name', 'county_name', 'year'])
        .vote_count
        .sum()
    )[state, county]
    
    X20 = votes.index.values
    X = X20[:-1]
    Y20 = votes.values
    Y = Y20[:-1]
    if X.size:
        coef = np.polyfit(X, Y, degree)
        poly = np.poly1d(coef)
        return X, Y, poly(X20), Y20

        dy = poly(X20) - Y20
        return dy
    return None, None, None, None




@click.command()
@click.option('--loglevel', default='info')
def main(loglevel):
    larc.logging.setup_logging(loglevel)

# @main.command()
# def 

if __name__ == '__main__':
    main()

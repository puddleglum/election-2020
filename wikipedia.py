import re
import time
import random

import larc
import larc.common as __
import toolz.curried as _
import requests
import bs4
import numpy as np
import pandas as pd

import utils

log = larc.logging.new_log(__name__)

state_url_names = {
    'alabama': 'Alabama',
    'alaska': 'Alaska',
    'arizona': 'Arizona',
    'arkansas': 'Arkansas',
    'california': 'California',
    'colorado': 'Colorado',
    'connecticut': 'Connecticut',
    'delaware': 'Delaware',
    'district of columbia': 'the_District_of_Columbia',
    'florida': 'Florida',
    'georgia': 'Georgia',
    'hawaii': 'Hawaii',
    'idaho': 'Idaho',
    'illinois': 'Illinois',
    'indiana': 'Indiana',
    'iowa': 'Iowa',
    'kansas': 'Kansas',
    'kentucky': 'Kentucky',
    'louisiana': 'Louisiana',
    'maine': 'Maine',
    'maryland': 'Maryland',
    'massachusetts': 'Massachusetts',
    'michigan': 'Michigan',
    'minnesota': 'Minnesota',
    'mississippi': 'Mississippi',
    'missouri': 'Missouri',
    'montana': 'Montana',
    'nebraska': 'Nebraska',
    'nevada': 'Nevada',
    'new hampshire': 'New_Hampshire',
    'new jersey': 'New_Jersey',
    'new mexico': 'New_Mexico',
    'new york': 'New_York',
    'north carolina': 'North_Carolina',
    'north dakota': 'North_Dakota',
    'ohio': 'Ohio',
    'oklahoma': 'Oklahoma',
    'oregon': 'Oregon',
    'pennsylvania': 'Pennsylvania',
    'rhode island': 'Rhode_Island',
    'south carolina': 'South_Carolina',
    'south dakota': 'South_Dakota',
    'tennessee': 'Tennessee',
    'texas': 'Texas',
    'utah': 'Utah',
    'vermont': 'Vermont',
    'virginia': 'Virginia',
    'washington': 'Washington_(state)',
    'west virginia': 'West_Virginia',
    'wisconsin': 'Wisconsin',
    'wyoming': 'Wyoming',
}

defunct_counties = {
    ('south dakota', 'armstrong'),
    ('south dakota', 'washabaugh'),
    ('south dakota', 'washington'),
    ('virginia', 'elizabeth city'),
    ('virginia', 'nansemond'),
    ('virginia', 'princess anne'),
    ('virginia', 'clifton forge'),
    ('virginia', 'south norfolk'),
    ('virginia', 'warwick'),
    ('georgia', 'campbell'),
    ('georgia', 'milton'),
    ('tennessee', 'james'),
}

county_name_lut = {
    ('florida', 'dade'): 'miami-dade',
    ('florida', 'de soto'): 'desoto',
    ('florida', "st. john's"): 'st. johns',
    ('florida', "st john's"): 'st. johns',
    ('florida', "st johns"): 'st. johns',
    ('florida', "st lucie"): 'st. lucie',
    ('illinois', 'dewitt'): 'de witt',
    ('georgia', 'de kalb'): 'dekalb',
    ('indiana', 'de kalb'): 'dekalb',
    ('louisiana', 'desoto'): 'de soto',
    ('louisiana', 'lasalle'): 'la salle',
    # ('louisiana', 'st. bernard'): 'st. bernard',
    # ('louisiana', 'st. charles'): 'st. charles',
    # ('louisiana', 'st. helena'): 'st. helena',
    # ('louisiana', 'st. james'): 'st. james',
    # ('louisiana', 'st. john the baptist'): 'st. john the baptist',
    # ('louisiana', 'st. landry'): 'st. landry',
    # ('louisiana', 'st. martin'): 'st. martin',
    # ('louisiana', 'st. mary'): 'st. mary',
    # ('louisiana', 'st. tammany'): 'st. tammany',
    ('indiana', 'la porte'): 'laporte',
    ('maryland', 'baltimore county'): 'baltimore',
    ('minnesota', 'saint louis'): 'st. louis',
    ('missouri', 'saint charles'): 'st. charles',
    ('missouri', 'saint clair'): 'st. clair',
    ('missouri', 'saint francois'): 'st. francois',
    ('missouri', 'saint louis county'): 'st. louis',
    ('missouri', 'saint louis'): 'st. louis',
    ('missouri', 'saint louis city'): 'st. louis city',
    ('missouri', 'sainte genevieve'): 'ste. genevieve',
    ('nevada', 'ormsby'): 'carson city',
    ('new hampshire', 'coös'): 'coos',
    ('new mexico', 'doña ana'): 'dona ana',
    ('south dakota', 'shannon'): 'oglala lakota',
    ('texas', 'de witt'): 'dewitt',
    ('virginia', 'albermarle'): 'albemarle',
}


def map_county_name(state, county):
    if '[' in county:
        county = county[:county.index('[')]

    county = county_name_lut.get((state, county), county)

    return (state, county)

@_.curry
def state_url(year, state):
    return (
        'https://en.wikipedia.org/wiki/'
        '{}_United_States_presidential_election_in_{}'
    ).format(year, state_url_names[state])

def county_fips():
    import county
    df = county.pres_data_historical()
    return (
        df[['state_name', 'county_name', 'county_fips']]
        .groupby(['state_name', 'county_name'])
        .county_fips
        .nth(0)
    )

def convert_county_name(state, county):
    if state == 'virginia':
        if county.endswith(' county'):
            county = county.replace(' county', '')
        elif county.endswith(' city'):
            county = county.replace(' city', '')

    if state == 'louisiana':
        county = (
            county
            .replace('st ', 'st. ')
            .replace('saint ', 'st. ')
            .replace('east.', 'east')
            .replace('west.', 'west')
        )

    state, county = map_county_name(state, county)
    return state, county

def get_county_fips(state, county):
    state, county = convert_county_name(state, county)

    if (state, county) in defunct_counties:
        return 0

    return county_fips().loc[state, county]

@_.curry
def county_row(year, state, parties, raw):
    if len(raw) == 6:         # main 1956 @#$@#%@#
        county, *candidates, _total = raw
    elif len(raw) == 7:         # california 1956 @#$@#%@#
        county, *candidates = raw
    else:
        county, *candidates, _margin_n, _margin_p, _total = raw

    return _.pipe(
        _.concatv(
            [county.strip().lower()],
            [(p, ) + cand_parse(t)
             for p, t in zip(parties, _.partition(2, candidates))],
        ),
        tuple,
    )

# @_.memoize
def get_results_span(soup):
    return _.pipe(
        (
            soup.find_all('span', text='Results by county'),
            soup.find_all(
                'span', text='Results by county or independent city'
            ),
            soup.find_all('span', text='Results by parish'),
            soup.find_all('span', text='County Results'),
        ),
        _.concat,
        _.filter(None),
        _.filter(lambda s: s.parent),
        _.filter(lambda s: s.parent.name in {'h1', 'h2', 'h3', 'h4', 'h5'}),
        __.maybe_first,
    )

def has_results_span(func):
    def check_for_span(year, state, soup):
        span = get_results_span(soup)

        if not span:
            return False

        # table = span.parent.find_next_sibling('table')
        return func(year, state, soup)
    return check_for_span

def th_tuple(th):
    return _.pipe(
        th.prettify().splitlines(),
        _.map(lambda l: l.strip()),
        _.filter(lambda l: not l.startswith('<')),
        tuple,
    )

get_th_tuples = _.compose_left(
    get_results_span,
    lambda span: (
        span
        .parent
        .find_next_sibling('table')
        .find('tr')
        .find_all('th')
    ),
    lambda ths: _.pipe(
        ths,
        _.map(th_tuple),
        tuple,
    )
)

def has_th_tuples(func):
    def check_for_th_tuple(year, state, soup):
        th_tuples = get_th_tuples(soup)
        if not th_tuples:
            return False
        return func(year, state, soup)
    return check_for_th_tuple

# @has_results_span
# @has_th_tuples
# def is_florida_1948(year, state, soup):
#     T = get_th_tuples(soup)

#     return (
#         (len(T[0]) == 0) and
#         (len([t for t in T if len(t) == 1]) == 1) and
#         (len(T[-1]) == 1)
#     )

# def florida_1948_rows(year, state, soup):
#     T = get_th_tuples(soup)
#     for cand, *_, party in get_th_tuples(soup)[1:-2]:
#         pass
    

@has_results_span
@has_th_tuples
def is_california_special(year, state, soup):
    T = get_th_tuples(soup)
    return (
        (T[0] == ('County',)) and (T[-1] == ('Votes',)) and
        (len(T) % 2 == 1)
    )

def percent(p):
    p = p.strip()[:-1]
    if not p:
        return 0.0
    p = re.sub(r'[a-zA-Z]', '', p)
    p = re.sub(r'\[.*\]', '', p)
    return float(p) / 100

def count(c):
    c = c.replace(',', '').strip()
    c = re.sub(r'\[.*\]', '', c)
    if not c:
        return 0
    return int(c)

def party_map(p):
    if re.search(r'^republican', p, re.I):
        return 'gop'
    elif re.search(r'^democrat', p, re.I):
        return 'dem'
    return 'other'

def cand_parse(t):
    if type(t[0]) is int and type(t[1]) is float:
        return t

    if '%' in t[0] or ',' in t[1]:
        t = (t[1], t[0])
    c, p = t
    return (count(c), percent(p))

def candidates_row(county, parties, candidates):
    return _.pipe(
        _.concatv(
            [county.strip().lower()],
            [(p, ) + cand_parse(t)
             for p, t in zip(parties, _.partition(2, candidates))],
        ),
        tuple,
    )

def rows_from_table(year, state, soup, table_f, row_f):
    table = table_f(soup)

    return _.pipe(
        table.find('tbody').find_all('tr'),
        _.filter(lambda tr: tr.find('td')),
        _.map(lambda tr: tr.find_all('td')),
        _.map(lambda tds: [td.text for td in tds]),
        _.map(row_f),
        _.filter(None),
        list,
    )

def standard_get_table(soup):
    span = get_results_span(soup)
    return span.parent.find_next_sibling('table')

tv_re = re.compile(r'^total votes', re.I)
m_re = re.compile(r'^margin', re.I)

@has_results_span
@has_th_tuples
def is_standard_table(year, state, soup):
    T = get_th_tuples(soup)
    log.debug(f'is_standard_table T: {T}')
    # county_column = False
    # if T[0]:
    #     county_column = bool(re.search(r'(county|parish)', T[0][0], re.I))
    return (
        len(T) >= 4 and
        # county_column and
        # len(T[-2]) == 1 and
        # len(T[-1]) == 1 and
        (
            m_re.search(T[-1][0]) or
            (m_re.search(T[-2][0]) and tv_re.search(T[-1][0])) or
            tv_re.search(T[-1][0])
        )
    )

parties_lookup_table = {
    (1952, 'mississippi'): ('dem', 'gop'),
    (1948, 'alabama'): ('other', 'gop', 'other', 'other'),
    (1948, 'mississippi'): ('other', 'gop', 'dem', 'other'),
}

def standard_rows(year, state, soup):
    T = get_th_tuples(soup)

    end_items = {
        tv_re: 1,
        m_re: 2
    }

    if m_re.search(T[-2][0]):
        T_end = -2
        cand_end = -3
    else:  # if re.search('margin|votes', T[-1][0], re.I):
        T_end = -1
        if m_re.search(T[-1][0]):
            cand_end = -2
        else:
            cand_end = -1

    parties = parties_lookup_table.get((year, state)) or _.pipe(
        T[1: T_end],
        _.map(_.last),
        _.map(party_map),
        tuple,
    )
    log.debug(parties)

    def row(t):
        county, *rest = t
        candidates = rest[: cand_end]
        return candidates_row(county, parties, candidates)

    if year == 1948:
        #if state in {'alabama', 'mississippi', 'louisiana', 'south carolina'}:
        if state == 'alabama':
            # Alabama did not have Truman on the ballot
            def row_al_1948(t, row=row):
                r = list(row(t))
                # add Truman as zeros
                r.insert(1, ('dem', 0, 0.0))
                return tuple(r)
            row = row_al_1948


    return rows_from_table(year, state, soup, standard_get_table, row)

def california_rows(year, state, soup):
    T = get_th_tuples(soup)

    if utils.winner_parties[year] == 'republican':
        major = ('gop', 'dem')
    else:
        major = ('dem', 'gop')

    parties = _.pipe(
        _.concatv(
            major,
            ['other' for _t in T[5::2]],
        ),
        tuple,
    )

    def row(t):
        county, *candidates = t
        return candidates_row(county, parties, candidates)

    return rows_from_table(year, state, soup, standard_get_table, row)


def delaware_table(soup):
    b = soup.find('b', text='County')
    if not b:
        return None
    header_tr = b.parent.parent
    tds = header_tr.find_all('td')
    if tds and tds[-1].text.strip() == 'Oth':
        return header_tr.parent.parent
    
def is_delaware_table(year, state, soup):
    table = delaware_table(soup)
    if table and table.name == 'table':
        return True
    return False

def row_data(tr):
    return _.pipe(
        tr.find_all('td'),
        _.map(lambda td: td.text.strip()),
        tuple,
    )

def delaware_rows(year, state, soup):
    table = delaware_table(soup)

    def row(t):
        name = t[0].lower().strip()

        dv, rv, ov = _.pipe(t[1:], _.map(count))
        log.info(f'{dv} {rv} {ov}')
        total = dv + rv + ov

        dp, rp, op = _.pipe([dv, rv, ov], _.map(lambda v: v / total))
        return name, ('dem', dv, dp), ('gop', rv, rp), ('other', ov, op)

    return _.pipe(
        table.find_all('tr')[1:-1],
        _.map(row_data),
        _.filter(lambda t: all(t)),
        _.map(row),
        tuple,
    )
    

table_templates = {
    # 'florida_1948': {
    #     'check': is_florida_1948,
    #     'rows': florida_1948_rows,
    # },
    'california_special': {
        'check': is_california_special,
        'rows': california_rows,
    },
    'standard': {
        'check': is_standard_table,
        'rows': standard_rows,
    },
    'delaware': {
        'check': is_delaware_table,
        'rows': delaware_rows,
    },
}

def get_table_rows(year, state, soup):
    log.info(f'Guessing table structure for {state} in {year}')
    if soup:
        for name, tdata in table_templates.items():
            if tdata['check'](year, state, soup):
                log.info(f'   found: {name}')
                return tdata['rows'](year, state, soup)
        log.error(f'Could not find a table template for {state} in {year}')
    else:
        log.error(f'Invalid HTML for {state} in {year}')
    return []


# @_.memoize
def state_data(year, state):
    try:
        log.info(f'Getting Wikipedia data for {state} in {year}')
        url = state_url(year, state)
        log.info(f'   url: {url}')
        resp = requests.get(
            url,
            headers={'user-agent': (
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                'AppleWebKit/537.36 (KHTML, like Gecko) Brave '
                'Chrome/86.0.4240.198 Safari/537.36'
            )},
        )

        if resp.status_code not in range(200, 300):
            log.error(f'Failure to access wikipedia for {state} in {year}: '
                      f'{resp.status_code} {resp.content[:400]}')
            return []

        soup = bs4.BeautifulSoup(
            resp.content, 'lxml'
        )

        return get_table_rows(year, state, soup)
    except:
        log.error(f'Problem getting data for {state} in {year}')
        raise

@_.curry
def state_df(year, state):
    log.info(f'Building DataFrame for {state} in {year}')
    raw_data = [
        (get_county_fips(state, row[0]),) + row
        for row in state_data(year, state)
    ]
    if not raw_data:
        return pd.DataFrame()

    party_order = {
        'dem': 0,
        'gop': 1,
        'other': 2,
    }
    for i, row in enumerate(raw_data):
        fips, county, *candidates = row
        
        candidates = sorted(candidates, key=lambda t: party_order[t[0]])

        row = [fips, county] + candidates

        all_votes = _.pipe(
            candidates,
            _.map(_.second),
            tuple,
        )
            
        total = sum(all_votes)
        
        other_votes = sum(all_votes[2:])
        other_percent = other_votes / total

        raw_data[i] = _.pipe(
            _.concatv(
                [row[0], row[1]],  # fips, county
                row[2][1:],        # dem
                row[3][1:],        # gop
                (other_votes, other_percent),
            ),
            tuple,
        )

    N = len(raw_data)

    dem_votes = _.pipe(
        raw_data,
        _.map(_.nth(2)),
        tuple,
        np.array,
    )
    gop_votes = _.pipe(
        raw_data,
        _.map(_.nth(4)),
        tuple,
        np.array,
    )
    other_votes = _.pipe(
        raw_data,
        _.map(_.nth(6)),
        tuple,
        np.array,
    )
    votes = _.pipe(
        raw_data,
        _.map(lambda r: sum(r[2::2])),
        tuple,
        np.array,
    )

    vote_count = np.concatenate([dem_votes, other_votes, gop_votes])
    total_votes = np.concatenate([votes, votes, votes])
    vote_percent = vote_count / total_votes

    winner = utils.winner_parties[year]
    if winner == 'republican':
        is_national_winner = (
            [False] * N + [False] * N + [True] * N
        )
        national_party_count = [gop_votes.sum()] * N * 3
    else:
        is_national_winner = (
            [True] * N + [False] * N + [False] * N
        )
        national_party_count = [dem_votes.sum()] * N * 3

    data = {
        'county_fips': _.pipe(
            raw_data,
            _.map(_.first),
            tuple,
            lambda t: t * 3,
        ),
        'county_name': _.pipe(
            raw_data,
            _.map(_.second),
            tuple,
            lambda t: t * 3,
        ),
        'state_name': [state] * N * 3,
        'year': [year] * N * 3,
        'party': ['D'] * N + ['O'] * N + ['R'] * N,
        'vote_count': vote_count,
        'vote_percent': vote_percent,
        'is_national_winner': is_national_winner,
        'national_party_count': national_party_count,
        'national_count': (
            [dem_votes.sum() + gop_votes.sum() + other_votes.sum()] * (N * 3)
        ),
    }
    # return data
    # print(data)

    df = pd.DataFrame(data)
    df['national_party_percent'] = (
        df['national_party_count'] / df['national_count']
    )
    df = df[~(df.county_fips == 0)]
    df = df.astype({'county_fips': int})
    return df

    # return raw_data

    # data = {
    #     'county_fips': dup(orig.fips),
    #     'state_name': dup(orig.state.str.lower()),
    #     'county_name': dup(orig.name.str.lower()),
    #     'year': [2020] * (N * 3),
    #     'party': ['D'] * N + ['O'] * N + ['R'] * N,
    #     'vote_count': pd.concat([biden_v, other_v, trump_v]),
    #     'vote_percent': pd.concat([biden_p, other_p, trump_p]),
    #     'is_national_winner': [True] * N + [False] * N + [False] * N,
    #     'national_party_count': [biden_v.sum()] * (N * 3),
    #     'national_count': (
    #         [biden_v.sum() + trump_v.sum() + other_v.sum()] * (N * 3)
    #     ),
    # }
    
    # df = pd.DataFrame(data)
    # df['national.party.percent'] = (
    #     df['national_party_count'] / df['national_count']
    # )

def all_states_data(year):
    df = _.pipe(
        state_url_names,
        # __.shuffled,
        # _.take(5),
        sorted,
        _.map(lambda state: (time.sleep(random.randint(1, 5)),
                             state_df(year, state))[-1]),
        # larc.parallel.thread_map(state_df(year), max_workers=5),
        lambda L: pd.concat(L, ignore_index=True),
    )
    df['national_party_count'] = (
        df[df.party == 'D']
        .groupby('state_name')
        .national_party_count
        .max()
        .sum()
    )
    df['national_count'] = (
        df[df.party == 'D']
        .groupby('state_name')
        .national_count
        .max()
        .sum()
    )
    return df

def all_states_url(year):
    return (
        'https://en.wikipedia.org/wiki/'
        '{}_United_States_presidential_election'
    ).format(year)

def all_states_urls(year):
    soup = bs4.BeautifulSoup(
        requests.get(all_states_url(year)).content, 'lxml'
    )

    return _.pipe(
        soup.find_all(
            'a', {
                'href': re.compile(
                    'United_States_presidential_election_in'
                )
            }
        ),
        _.filter(lambda a: a.parent.name == 'td'),
        _.map(lambda a: (a.text, 'https://en.wikipedia.org' + a['href'])),
        dict,
    )



def county_data(year):
    pass

